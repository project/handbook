<?php
/**
 * @file
 * Overrides and preprocess functions for our theme.
 */

/**
 * Implementation of HOOK_theme().
 */
function handbook_theme(&$existing, $type, $theme, $path) {
  // The following line is needed to get Zen to work properly.
  $hooks = zen_theme($existing, $type, $theme, $path);

  return $hooks;
}

/**
 * Process variables for page.tpl.php.
 */
function handbook_preprocess_page(&$variables) {
  // Link the site name to the frontpage if we're not on the frontpage.
  if (!$variables['is_front']) {
    $variables['site_name'] = l($variables['site_name'], '<front>', array(
      'attributes' => array(
          'title' => t('Home'),
          'rel' => 'home',
        ),
    ));
  }

  // We render node meta variables here so we can print them in the sidebar.
  $node = menu_get_object();
  $variables['node_changed'] = '';
  if ($node && theme_get_setting('toggle_node_info_' . $node->type) && $node->changed) {
    $variables['node_changed'] = format_date($node->changed);
  }
  $variables['node_terms'] = '';
  if (module_exists('taxonomy')) {
    $taxonomy = taxonomy_link('taxonomy terms', $node);
    $variables['node_terms'] = theme('links', $taxonomy);
  }
}
