<?php
/**
 * @file
 * Page template.
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body class="<?php print $classes; ?>">

  <div id="page-wrapper"><div id="page">

    <div id="header"><div class="section clearfix">
      <?php if ($site_name || $header): ?>
        <div id="header-top" class="clearfix">
          <?php if ($site_name): ?>
            <?php if ($title): ?>
              <div id="site-name"><strong>
                <?php print $site_name; ?>
              </strong></div>
            <?php else: /* Use h1 when the content title is empty */ ?>
              <h1 id="site-name">
                <?php print $site_name; ?>
              </h1>
            <?php endif; ?>
          <?php endif; ?>

          <?php print $header; ?>
        </div>
      <?php endif; ?>

      <?php print theme(array('links__system_main_menu', 'links'), $primary_links,
        array(
          'id' => 'main-menu',
          'class' => 'links clearfix',
        ),
        array(
          'text' => t('Main menu'),
          'level' => 'h2',
          'class' => 'element-invisible',
        ));
      ?>
    </div></div> <!-- /.section, /#header -->

    <?php if ($secondary_links): ?>
      <div id="header-bottom-wrapper"><div id="header-bottom">
        <?php print theme(array('links__system_secondary_menu', 'links'), $secondary_links,
          array(
            'id' => 'secondary-menu',
            'class' => 'links clearfix',
          ),
          array(
            'text' => t('Secondary menu'),
            'level' => 'h2',
            'class' => 'element-invisible',
          ));
        ?>
      </div></div> <!-- /#header-bottom, /#header-bottom-wrapper -->
    <?php endif; ?>

    <div id="main-wrapper"><div id="main" class="clearfix">

      <?php if ($messages): ?>
        <div id="console">
          <?php print $messages; ?>
        </div>
      <?php endif; ?>

      <?php print $content_top; ?>

      <div id="content"><div class="section">

        <?php if ($title): ?>
          <h1 class="title"><?php print $title; ?></h1>
        <?php endif; ?>

        <?php if ($is_front && $site_slogan): ?>
          <h2 id="site-slogan" class="title"><?php print $site_slogan; ?></h2>
        <?php endif; ?>

        <?php if ($tabs): ?>
          <div class="tabs"><?php print $tabs; ?></div>
        <?php endif; ?>

        <?php print $help; ?>
        <?php print $content; ?>

      </div></div> <!-- /.section, /#content -->

      <div id="sidebar">
        <?php print $sidebar_first; ?>

        <?php if ($node_changed || $node_terms): ?>
          <div class="node-meta">
            <?php if ($node_changed): ?>
              <div class="changed">
                <h3><?php print t('Last revision'); ?></h3>
                <?php print $node_changed; ?>
              </div>
            <?php endif; ?>

            <?php if ($node_terms): ?>
              <div class="terms">
                <h3><?php print t('Tags'); ?></h3>
                <?php print $node_terms; ?>
              </div>
            <?php endif; ?>
          </div>
        <?php endif; ?>
      </div>

    </div></div> <!-- /#main, /#main-wrapper -->

    <?php print $content_bottom; ?>

    <?php if ($footer): ?>
      <div id="footer"><div class="section">
        <?php print $footer; ?>
      </div></div> <!-- /.section, /#footer -->
    <?php endif; ?>

  </div></div> <!-- /#page, /#page-wrapper -->

  <?php print $closure; ?>

</body>
</html>
